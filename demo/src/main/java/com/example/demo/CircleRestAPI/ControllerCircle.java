package com.example.demo.CircleRestAPI;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ControllerCircle {
    @GetMapping("/circle-area")

    public double getCircleArea(){

        double radius1 = 3.0 ;
        double radius2 = 5.0 ;
        double radius3 = 7.0 ;
          

        ArrayList<Double> r = new ArrayList<Double>();

        r.add(radius1);
        r.add(radius2);
        r.add(radius3);

        return r.size();
    }
}
